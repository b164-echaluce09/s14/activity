console.log ('Hello World')

let fName = 'Carlo Magno'
console.log("First Name:" + fName);

let lName = 'Echaluce'
console.log("Last Name:" + lName);

let age = 38
console.log("Age:" + age)

function  printUserInfo(fname,lName, age){
	console.log(fname, lName, 'is', age)
}

let hobbies = ["singing", "clarinet", "cooking"];
console.log(hobbies);

let workAddress = {
	houseNumber: "329",
	street: "Fortunato Dungo Street",
	city: "Apalit"
}

console.log(workAddress)

function printUserInfo(fName, lName, age) {
	console.log(`${fName} ${lName} is ${age} years of age.`)
	console.log(`This was printed inside of the function`)
	console.log(hobbies)
	console.log(workAddress)
}

printUserInfo(fName, lName, age)

function returnFunction() {
	return true;
}

let isMarried = returnFunction();

console.log(`The value of isMarried is: ${isMarried}`)


function addition(a, b){
	return a + b
}

let add = addition(4, 5)
console.log(add);


const subNum1 = parseInt(prompt('Get the Difference: Enter the first number '));
const subNum2 = parseInt(prompt('Get the Difference: Enter the second number '));

const difference = subNum1 - subNum2;

console.log(`The difference of ${subNum1} and ${subNum2} is ${difference}`);